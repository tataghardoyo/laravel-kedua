<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('items.listcast', compact('cast'));
    }

    public function create(){
        return view('items.tambahcast');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('items.detailcast', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('items.editcast', compact('cast'));
    }

    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $cast_id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($cast_id){
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast');
    }
}
