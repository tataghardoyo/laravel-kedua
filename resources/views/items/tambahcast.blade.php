@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>tambah cast</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/cast')}}">cast</a></li>
                    <li class="breadcrumb-item active">tambah cast</li>
                </ol>
            </div>
        </div>

    </div>

</section>
<section class="content">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">form tambah cast</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                    title="Remove">
                    <i class="fas fa-times"></i></button>
            </div>
        </div>
        <div class="card-body">
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama Cast</label>
                    <input type="text" class="form-control" name="nama" id="nama"
                        placeholder="masukkan nama cast">
                    @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Umur Cast</label>
                    <input type="number" class="form-control" name="umur" id="umur"
                        placeholder="masukkan umur cast">
                    @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Bio Cast</label>
                    <textarea class="form-control" name="bio" id="bio" rows="3"></textarea>
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <input class="btn btn-success" type="submit" value="Tambah">
            </form>
        </div>
    </div>
</section>
@endsection